
doom_idle_spine.png
size: 392,27
format: RGBA8888
filter: Linear,Linear
repeat: none
Layer 1
  rotate: false
  xy: 2, 2
  size: 15, 23
  orig: 17, 24
  offset: 1, 1
  index: -1
Layer 2
  rotate: false
  xy: 32, 2
  size: 28, 21
  orig: 30, 22
  offset: 1, 1
  index: -1
Layer 3
  rotate: false
  xy: 62, 2
  size: 24, 21
  orig: 26, 22
  offset: 1, 1
  index: -1
Layer 4
  rotate: false
  xy: 92, 2
  size: 16, 23
  orig: 18, 24
  offset: 1, 1
  index: -1
Layer 5
  rotate: false
  xy: 122, 2
  size: 16, 23
  orig: 18, 24
  offset: 1, 1
  index: -1
Layer 6
  rotate: false
  xy: 152, 2
  size: 17, 22
  orig: 19, 23
  offset: 1, 1
  index: -1
body
  rotate: false
  xy: 182, 2
  size: 15, 14
  orig: 17, 15
  offset: 1, 0
  index: -1
hand_b1
  rotate: false
  xy: 212, 2
  size: 6, 10
  orig: 8, 12
  offset: 1, 1
  index: -1
hand_b2
  rotate: false
  xy: 242, 2
  size: 6, 6
  orig: 8, 8
  offset: 1, 1
  index: -1
hand_t1
  rotate: false
  xy: 272, 2
  size: 6, 12
  orig: 8, 14
  offset: 1, 1
  index: -1
hand_t2
  rotate: false
  xy: 302, 2
  size: 6, 6
  orig: 8, 8
  offset: 1, 1
  index: -1
head
  rotate: false
  xy: 332, 2
  size: 12, 11
  orig: 14, 13
  offset: 1, 1
  index: -1
weapon
  rotate: false
  xy: 362, 2
  size: 27, 9
  orig: 29, 11
  offset: 1, 1
  index: -1
