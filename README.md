To use the spine-unity runtime in your Unity project:

1 Download and install "Unity version 2020.1.2f1"

2 Create a new empty project in the "Unity Editor" or "Unity Hub"

3 Download the latest spine-unity unitypackage http://ru.esotericsoftware.com/spine-unity-download

(Alternatively you can get the latest changes via Git https://github.com/esotericsoftware/spine-runtimes)

Import Spine unitypackage (you can double-click on it and Unity will open it).

4 URP Support Shaders 

Window -> Package Manager -> PackageManager Spine Lightweight RP Shaders

unitypackage https://esotericsoftware.com/files/runtimes/unity/com.esotericsoftware.spine.urp-shaders-3.8-Unity2019.3-2021-03-04.zip

optional 5: Timeline support unitypackage https://esotericsoftware.com/files/runtimes/unity/com.esotericsoftware.spine.timeline-3.8-2021-03-19.zip